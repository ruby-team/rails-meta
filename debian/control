Source: rails
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>,
           Antonio Terceiro <terceiro@debian.org>
Standards-Version: 3.9.7
Build-Depends: debhelper (>= 9~)
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/rails-meta.git
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/rails-meta.git

Package: rails
Architecture: all
Depends: ruby-rails-3.2,
         ${misc:Depends}
Description: MVC ruby based framework geared for web application development
 Rails is a full-stack, open-source web framework in Ruby for writing
 real-world applications.
 .
 Being a full-stack framework means that all layers are built to work
 seamlessly together. That way you don't repeat yourself and you can
 use a single language from top to bottom. Everything from templates to
 control flow to business logic is written in Ruby.
 .
 This is (almost) empty dependency package which provide the rails
 command which runs the defaults version of the rails.  If you need
 a specific version of rails, you need to depend on ruby-rails-VERSION
 package.

Package: ruby-activemodel
Architecture: all
Depends: ruby-activerecord-3.2,
         ${misc:Depends}
Description: toolkit for building modeling frameworks (part of Rails)
 Active Model is a toolkit for building modeling frameworks like
 Active Record and Active Resource.  This includes a rich support for
 attributes, callbacks, validations, observers, serialization,
 internationalization, and testing.
 .
 This is empty dependency package which depends on the default Debian
 version of rails framework.  If you need to depend on a specific version
 of rails, you need to depend on <package>-VERSION package.

Package: ruby-activerecord
Architecture: all
Depends: ruby-activerecord-3.2,
         ${misc:Depends}
Description: ORD database interface for ruby
 Implements the ActiveRecord pattern (Fowler, PoEAA) for ORM.  It ties
 database tables and classes together for business objects, like
 Customer or Subscription, that can find, save, and destroy themselves
 without resorting to manual SQL.
 .
 This is empty dependency package which depends on the default Debian
 version of rails framework.  If you need to depend on a specific version
 of rails, you need to depend on <package>-VERSION package.

Package: ruby-activeresource
Architecture: all
Depends: ruby-activerecord-3.2,
         ${misc:Depends}
Description: Connects objects and REST web services
 Active Resource (ARes) connects business objects and Representational
 State Transfer (REST) web services. It implements object-relational
 mapping for REST webservices to provide transparent proxying
 capabilities between a client (ActiveResource) and a RESTful service
 (which is provided by Simply RESTffull routing in
 ActionController::Resources)
 .
 This is empty dependency package which depends on the default Debian
 version of rails framework.  If you need to depend on a specific version
 of rails, you need to depend on <package>-VERSION package.

Package: ruby-activesupport
Architecture: all
Depends: ruby-activesupport-3.2,
         ${misc:Depends}
Description: Support and utility classes used by the Rails framework
 ActiveSupport consists of utility classes and extensions to the Ruby
 standard library that were required for Rails but found to be
 generally useful.
 .
 This is empty dependency package which depends on the default Debian
 version of rails framework.  If you need to depend on a specific version
 of rails, you need to depend on <package>-VERSION package.

Package: ruby-actionpack
Architecture: all
Depends: ruby-actionpack-3.2,
         ${misc:Depends}
Description: Controller and View framework used by Rails
 Action Pack splits the response to a requests into a controller part
 (performing the logic) and a view part (rendering a template). This
 two-step approach is known as an action, which will normally create,
 read, update, or delete (CRUD for short) some sort of model part
 (often backed by a database) before choosing either to render a
 template or redirecting to another action.
 .
 This is empty dependency package which depends on the default Debian
 version of rails framework.  If you need to depend on a specific version
 of rails, you need to depend on <package>-VERSION package.

Package: ruby-actionmailer
Architecture: all
Depends: ruby-actionmailer-3.2,
         ${misc:Depends}
Description: Framework for generation of customized email messages
 Action Mailer is a framework for designing email-service layers. These layers
 are used to consolidate code for sending out forgotten passwords, welcome
 wishes on signup, invoices for billing, and any other use case that requires
 a written notification to either a person or another system.
 .
 This is empty dependency package which depends on the default Debian
 version of rails framework.  If you need to depend on a specific version
 of rails, you need to depend on <package>-VERSION package.

Package: ruby-railties
Architecture: all
Depends: ruby-railties-3.2,
         ${misc:Depends}
Description: MVC ruby based framework geared for web application development
 Rails is a full-stack, open-source web framework in Ruby for writing
 real-world applications.
 .
 Being a full-stack framework means that all layers are built to work
 seamlessly together. That way you don't repeat yourself and you can
 use a single language from top to bottom. Everything from templates to
 control flow to business logic is written in Ruby.
 .
 This is empty dependency package which depends on the default Debian
 version of rails framework.  If you need to depend on a specific version
 of rails, you need to depend on <package>-VERSION package.
